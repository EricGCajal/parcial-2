using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private PlayerInput playerInput;
    public PlayerInput.On_FootActions On_Foot;
    private PlayerMotor motor;
    private PlayerLook look;

    // Start is called before the first frame update
    void Awake()
    {
        playerInput = new PlayerInput();
        On_Foot = playerInput.On_Foot;
        motor = GetComponent<PlayerMotor>();
        look = GetComponent<PlayerLook>();
        On_Foot.Jump.performed += ctx => motor.Jump();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Le decimos a PlayerMotor que se mueva usando los valores de nuestras acciones de movimiento
        motor.ProcessMove(On_Foot.Movement.ReadValue<Vector2>());
    }

    private void LateUpdate()
    {
        look.ProcessLook(On_Foot.Look.ReadValue<Vector2>());
    }
    private void OnEnable()
    {
        On_Foot.Enable();
    }
    private void OnDisable()
    {
        On_Foot.Disable();
    }
}
