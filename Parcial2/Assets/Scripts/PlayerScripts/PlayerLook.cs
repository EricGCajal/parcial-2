using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Camera cam;
    private float RotationX = 0f;
    public float SensitivityX = 20f;
    public float SensitivityY = 20f;
    
    public void ProcessLook(Vector2 input)
    {
        float MouseX = input.x;
        float MouseY = input.y;
        RotationX -= (MouseY * Time.deltaTime) * SensitivityY;
        RotationX = Mathf.Clamp(RotationX, -60f, 60f);
        cam.transform.localRotation = Quaternion.Euler(RotationX, 0, 0);
        transform.Rotate(Vector3.up*(MouseX * Time.deltaTime) * SensitivityX);

    }
}
