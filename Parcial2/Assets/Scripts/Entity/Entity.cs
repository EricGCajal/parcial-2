using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Entity : MonoBehaviour
{
    public GameObject Target;
    public NavMeshAgent Agent;
    public int Range;

    void Update()
    {
        float dist = Vector3.Distance(this.transform.position, Target.transform.position);
        if(dist < Range)
        {
            Agent.destination = Target.transform.position;
        }
    }
}
